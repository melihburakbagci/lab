public class MyDateTime {

    MyDate date;

    MyTime time;

    public MyDateTime(MyDate date, MyTime time) {
        this.date = date;
        this.time = time;
    }

    public String toString() {
        return date + " " + time;
        // return date.toString() + " " + time.toString();
    }

    public void incrementDay() {
        date.incrementDay();
    }

    public void incrementHour() {
        incrementHour(1);
    }

    public void incrementHour(int diff) {
        int dayDiff = time.incermentHour(diff);
        if (dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);
    }

    public void decrementHour(int diff) {
        incrementHour(-diff);
    }

    public void incrementMinute(int diff) {

        int dayDiff = time.incrementMinute(diff);
        if (dayDiff<0)
            date.decrementDay(-dayDiff);
        else
            date.incrementDay(dayDiff);

    }

    public void decrementMinute(int diff) {
        incrementMinute(-diff);
    }

    public void incrementYear(int i) {
        date.incrementYear(i);
    }

    public void decrementDay() {
        date.decrementDay();
    }

    public void decrementYear() {
        date.decrementYear();
    }

    public void decrementMonth() {
        date.decrementMonth();
    }
    public void incrementDay (int i) {
        date.incrementDay(i);
    }

    public void incrementYear() {
        date.incrementYear();
    }

    public void decrementMonth(int i) {
        date.decrementMonth(i);
    }

    public void decrementDay(int i) {
        date.decrementDay(i);
    }

    public void incrementMonth(int i) {
        date.incrementMonth(i);
    }

    public void decrementYear(int i) {
        date.decrementYear(i);
    }

    public void incrementMonth() {
        date.incrementMonth();
    }

    public boolean isBefore(MyDateTime anotherDateTime) {
        if (time.hour < anotherDateTime.time.hour)
            return true;
        else if ((time.hour <= anotherDateTime.time.hour) && (time.minute < anotherDateTime.time.minute))
            return true;
        return false;
    }

    public boolean isAfter(MyDateTime anotherDateTime) {
        if (time.hour > anotherDateTime.time.hour)
            return true;
        else if ((time.hour >= anotherDateTime.time.hour) && (time.minute > anotherDateTime.time.minute))
            return true;
        return false;
    }

    public String dayTimeDifference(MyDateTime anotherDateTime) {
        if (isBefore(anotherDateTime)) {
            int a = time.minute;
            int b = time.hour;
            while (true){
                a++;
                if (a == 60){
                    b++;
                    a = 0;
                }
                if (b == anotherDateTime.time.hour)
                    break;
            }
            a = (anotherDateTime.time.minute > time.minute ? anotherDateTime.time.minute - time.minute : time.minute - anotherDateTime.time.minute);
            return b + " hour(s)" + a + " minute(s)";
        }else {
            int a = anotherDateTime.time.minute;
            int b = anotherDateTime.time.hour;
            while (true){
                a++;
                if (a == 60){
                    b++;
                    a = 0;
                }
                if (b == time.hour)
                    break;
            }
            a = (time.minute > anotherDateTime.time.minute ? time.minute - anotherDateTime.time.minute : anotherDateTime.time.minute - time.minute);
            return b + " hour(s)" + a + " minute(s)";
        }
    }
}
