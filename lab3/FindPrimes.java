public class FindPrimes {
    public static void main(String[] args){
        int max = Integer.parseInt(args[0]);

        //Print the numbers less than max
        //For each number less than max
        for (int number = 2; number < max; number++){
            //Let divisor =2;
            int divisor =2;
            //Let isprime ==true;
            boolean isprime = true;
            //While divisor less than number
            while (divisor < number && isprime){
                //If the number is divisible by the divisor
                if (number % divisor == 0)
                    isprime = false;
                //Increment divisor
                divisor++;
            }
            //If the numbers prime
            if (isprime)
                System.out.print(number + " "); //Print the number
        }
    }
}
