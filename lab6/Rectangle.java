public class Rectangle {

    private Point topLeft;
    private int width;
    private int height;


    public Rectangle(Point topLeft, int widht, int height) {
        this.topLeft = topLeft;
        this.width = widht;
        this.height = height;
    }

    public int area(){
        return width * height;
    }

    public int perimeter(){
        return 2 * (height + width);
    }

    public Point[] corners(){
        Point[] corners = new Point[4];
        corners[0] = topLeft;
        corners[1] = new Point(topLeft.getxCoord() + width, topLeft.getyCoord());
        corners[2] = new Point(topLeft.getxCoord(), topLeft.getyCoord() - height);
        corners[3] = new Point(topLeft.getxCoord() - width, topLeft.getyCoord());

        return corners;
    }

}

